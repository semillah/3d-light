#include "ofApp.h"

ofEasyCam cam; // Declare a 3D camera

//--------------------------------------------------------------
void ofApp::setup(){
    ofBackground(0); // Set the background to black
    ofSetFrameRate(120);
    ofEnableDepthTest(); // Enable depth testing for 3D rendering
}

//--------------------------------------------------------------
void ofApp::update(){
    // The update logic remains the same or can be modified for more complex animations
}

//--------------------------------------------------------------
void ofApp::draw(){
    cam.begin(); // Begin 3D camera

    // Define the grid pattern: 2, 4, 5, 2
    int gridPattern[4] = {12,9, 4, 13};
    float spacing = 70; // Space between lights

    // Strobe effect: Flicker between black and red
    float time = ofGetElapsedTimef();
    float flicker = sin(time * 50); // Adjust frequency for faster or slower strobe
    ofFloatColor strobeColor(flicker, 0, 0, 1); // Flicker between black (0,0,0) and red (1,0,0)

    for (int row = 0; row < 4; row++) {
        for (int light = 0; light < gridPattern[row]; light++) {
            drawLight(-spacing * (gridPattern[row]-1)/2 + light * spacing, -150 + row * spacing, strobeColor);
        }
    }

    cam.end(); // End 3D camera
    
    // Draw FPS counter
    ofSetColor(255);
    ofDrawBitmapString("FPS: " + ofToString(ofGetFrameRate()), 20, 20);
}

void ofApp::drawLight(float x, float y, ofFloatColor color){
    float time = ofGetElapsedTimef(); // Get the elapsed time for animation

    float rotationAngle = ofMap(sin(time * 15.5), -1, 1, 45, -45); // Rotates between 45 and -45 degrees

    float lightX = x;
    float lightY = y;
    float lightZ = 0;

    ofVec3f topPoint = ofVec3f(lightX, lightY, lightZ);
    ofVec3f leftPoint = topPoint + ofVec3f(-10, 600, -50);
    ofVec3f rightPoint = topPoint + ofVec3f(10, 600, 50);

    // Rotate points around the X-axis
    leftPoint = leftPoint.getRotated(rotationAngle, ofVec3f(1, 0, 0));
    rightPoint = rightPoint.getRotated(rotationAngle, ofVec3f(1, 0, 0));

    // Create and draw the triangle mesh for the light
    ofMesh triangleMesh;
    triangleMesh.setMode(OF_PRIMITIVE_TRIANGLES);
    
    triangleMesh.addVertex(topPoint);
    triangleMesh.addColor(color); // Red color at the top

    // Add the base vertices
    triangleMesh.addVertex(leftPoint);
    triangleMesh.addColor(ofFloatColor(0, 0, 0, 1)); // Black

    triangleMesh.addVertex(rightPoint);
    triangleMesh.addColor(ofFloatColor(0, 0, 0, 1)); // Black

    triangleMesh.draw(); // Draw the light
}

//--------------------------------------------------------------
void ofApp::exit(){

}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseScrolled(int x, int y, float scrollX, float scrollY){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
